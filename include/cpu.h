#pragma once

using namespace std;

#include <functional>



#include <BitUtil.h>

using namespace BitUtil;



#include <mmu.h>

class CPU
{
private:
	static const int MEMLEN = 0x10000;
	
	int y;
	int z;
	int p;
	int q;
	
	bool cpuStop;
	bool cpuHalt;
	
	bool eiBool;
	
	void nop();
	void ld_nn_sp();
	void stop();
	void jr();
	void jr_cc();
	void ld_rp();
	void add_hl();
	void ld_rp_a();
	void ld_hl_a();
	void ld_a_rp();
	void ld_a_hl();
	void inc_rp();
	void dec_rp();
	void inc_r();
	void dec_r();
	void ld_r_n();
	void rlca();
	void rrca();
	void rla();
	void rra();
	void daa();
	void cpl();
	void scf();
	void ccf();
	void ld_r_r();
	void halt();
	void alu_r();
	void ret_cc();
	void ld_io_a();
	void add_sp_d();
	void ld_a_io();
	void ld_hl_spd();
	void pop_rp2();
	void _ret();
	void reti();
	void jp_hl();
	void ld_sp_hl();
	void jp_cc_nn();
	void ld_ioc_a();
	void ld_nn_a();
	void ld_a_ioc();
	void ld_a_nn();
	void jp_nn();
	void di();
	void ei();
	void call_cc_nn();
	void push_rp2();
	void call_nn();
	void alu_n();
	void rst();
	void rot_r();
	void bit();
	void res();
	void set();
	
	void error();
	
	int cc(int index);
	void cc(int index, int value);
	void alu(int index, int operand);
	void rot(int index, unsigned char &operand);
	unsigned short rp(int index);
	void rp(int index, int value);
	unsigned short rp2(int index);
	void rp2(int index, int value);
	void call(int addr);
	void ret();
	void push(int value);
	short pop();
	void splitOpcode(unsigned char op);
	void updateMEMHL();
	void updateHLMEM();
	void flags(int flags, int operation, int fnum, int snum);
	
public:
	MMU mmu;
	bool ime;
	static const int IE = 0xFFFF;
	static const int IF = 0xFF0F;
	
	static const int A = 7;
	static const int B = 0;
	static const int C = 1;
	static const int D = 2;
	static const int E = 3;
	static const int H = 4;
	static const int L = 5;
	static const int R_HL = 6;
	static const int F = 8;
	
	static const int NZ = 0;
	static const int Z = 1;
	static const int NCA = 2;
	static const int CA = 3;
	static const int NE = 4;
	static const int HC = 5;
	
	// for use with the flags() method
	static const int F_Z = 0x01;
	static const int F_CA = 0x02;
	static const int F_NE = 0x04;
	static const int F_HC = 0x08;
	
	static const int AND = 2;
	static const int OR = 3;
	static const int XOR = 4;
	static const int CP = 5;
	
	// for use with either rp() or rp2()
	static const int BC = 0;
	static const int DE = 1;
	static const int HL = 2;
	
	// for use with rp()
	static const int SP = 3;
	
	// for use with rp2()
	static const int AF = 3;
	
	static const int VBL = 0;
	static const int STAT = 1;
	static const int TIMER = 2;
	static const int SERIAL = 3;
	static const int JOYPAD = 4;
	
	int t;
	long totalT;
	
	unsigned char opcode;
	
	unsigned short pc;
	unsigned short sp;
	
	signed char d;
	unsigned char n;
	unsigned short nn;
	
	unsigned char* r;
	
	function<void (void)> cpufunc[512];
	
	CPU();
	
	void init();
	void initMMU();
	void startBIOS();
	void intr(int intr);
	
	void cycle();
};