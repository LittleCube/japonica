#pragma once

class Tile
{
public:
	const static int LEN = 16;
	
	unsigned short index;
	
	Tile();
	
	Tile(unsigned short newIndex);
	
	void setIndex(unsigned short newIndex);
	
	int getPixel(int x, int y);
};