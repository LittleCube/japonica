#pragma once

class MMU
{
private:
	static const int BIOSLEN = 0x100;
	static const int ROMAREALEN = 0x8000;
	
	int mbcType;
	bool battery;
	
	int fullRomLen;
	bool bigRom;
	
	int ramLen;
	bool bigRam;
	
	char* fullrom;
	char* bios;
	
	ofstream save;
	
	MBC1 mbc1;
	
public:
	int romLength;
	
	int banks;
	
	unsigned char* memory;
	
	MMU();
	
	void init();
	
	unsigned char read(int addr);
	void readEffects(int addr, unsigned char &value);
	
	void write(int addr, unsigned char value);
	bool writeEffects(int addr, unsigned char &value);
	void writeBit(int position, int value, int addr);
	
	void dma(int addr);
	
	bool loadROM(string path, bool isBios);
	void clearBIOS();
};