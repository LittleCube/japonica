#pragma once

class Util
{
public:
	static void reset();
	static unsigned long getTime();
	static void msleep(long millisec);
	static void quit();
};