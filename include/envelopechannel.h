#pragma once

#include <channel.h>

class EnvelopeChannel : public Channel
{
public:
	unsigned char periodTimer;
	unsigned char currentVolume;
	
	EnvelopeChannel();
	void init();
	
	void triggerEvent();
	
	void step();
	
	void stepFrameSequencer();
	void performFrameStep();
};