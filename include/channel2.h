#pragma once

#include <tonechannel.h>

class Channel2 : public ToneChannel
{
public:
	void init();
	
	float* getSamples();
};