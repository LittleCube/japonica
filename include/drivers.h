#pragma once

class Drivers
{
private:
	bool quit = false;
	
public:
	static const int G_W = 160;
	static const int G_H = 144;
	
	Drivers();
	Drivers(int scale);
	
	void init(int scale);
	void initVideo(int scale);
	void initAudio();
	
	void fillAudio(float *buffer, int length);
	
	void setPixel(int x, int y, Color color);
	void setPixels(Color gfx[]);
	
	void clearDisplay();
	void fillDisplay(Color fill);
	
	void update();
	
	void exitGfx();
};