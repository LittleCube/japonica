#pragma once

class Sprite
{
public:
	unsigned char y;
	unsigned char x;
	
	unsigned char tileNo;
	
	int priority;
	
	int yflip;
	int xflip;
	
	int palette;
	
	Sprite();
	
	Sprite(int offset);
	
	void updateSprite(int offset);
};