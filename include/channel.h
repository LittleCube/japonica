#pragma once

#define NRX0 mmu.memory[ADDR_NRX0]
#define NRX1 mmu.memory[ADDR_NRX1]
#define NRX2 mmu.memory[ADDR_NRX2]
#define NRX3 mmu.memory[ADDR_NRX3]
#define NRX4 mmu.memory[ADDR_NRX4]

class Channel
{
public:
	int ADDR_NRX0;
	int ADDR_NRX1;
	int ADDR_NRX2;
	int ADDR_NRX3;
	int ADDR_NRX4;
	
	unsigned short timer;
	unsigned short frameSequencerTimer;
	int frameSequencerStep;
	
	void triggerEvent();
	
	void resetTimer();
	
	unsigned char lengthCounter;
	
	Channel();
	void init();
	
	void updateLength(unsigned char newNRX1);
	
	void step();
	
	void stepTimer();
	
	void stepFrameSequencer();
	void performFrameStep();
	
	void decLength();
	
	float* getSamples();
};