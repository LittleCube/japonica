#pragma once

#include <iostream>
#include <string>

#include <fstream>

#ifdef _3DS
#include <3ds.h>
#else
#include <SDL2/SDL.h>
#endif

#include <color.h>
#include <palette.h>
#include <tile.h>
#include <sprite.h>
#include <ppu.h>

#include <joypad.h>

#include <apu.h>

#include <mbc1.h>

#include <cpu.h>
#include <timer.h>

#include <drivers.h>

#include <util.h>

using namespace std;

#define mmu cpu.mmu

extern CPU cpu;
extern Timer timer;
extern PPU ppu;
extern APU apu;
extern Joypad joypad;

extern Drivers drivers;

extern bool useBIOS;

void setTitle();