#pragma once

class Color
{
public:
	int r;
	int g;
	int b;
	
	Color();
	
	Color(int newR, int newG, int newB);
};