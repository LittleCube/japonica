#pragma once

#include <tonechannel.h>

class Channel1 : public ToneChannel
{
public:
	bool sweepEnabled;
	
	unsigned short shadowFrequency;
	unsigned short sweepTimer;
	
	void init();
	
	void triggerEvent();
	
	void step();
	
	void stepFrameSequencer();
	void performFrameStep();
	
	void resetSweepTimer();
	unsigned char getSweepPeriod();
	unsigned short calcSweepFreq();
	unsigned char getSweepShift();
	void overflowCheck(unsigned short freq);
	
	float* getSamples();
};