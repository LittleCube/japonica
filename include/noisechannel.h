#pragma once

#include <channel.h>

class NoiseChannel : public EnvelopeChannel
{
public:
	unsigned short lfsr;
	
	void init();
	
	void triggerEvent();
	
	void step();
	void stepTimer();
	
	void resetTimer();
	unsigned char decodeDivisor(int divisorCode);
	
	int getSample();
};