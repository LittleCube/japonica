#pragma once

class Timer
{
private:
	unsigned short internal;
	
	int prevAND;
	
public:
	Timer();
	
	static const int DIV = 0xFF04;
	static const int TIMA = 0xFF05;
	static const int TMA = 0xFF06;
	static const int TAC = 0xFF07;
	
	void init();
	
	void clock(int clocks);
	int divBit();
	void incTima();
};