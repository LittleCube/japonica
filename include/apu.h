#pragma once

#include <channel1.h>
#include <channel2.h>
#include <channel3.h>
#include <channel4.h>

#include <envelopechannel.h>

#include <tonechannel.h>
#include <wavechannel.h>
#include <noisechannel.h>

class APU
{
private:
	static const int MAXCOUNTDOWN;
	
public:
	static const int NR50;
	static const int NR51;
	static const int NR52;
	
	static const int BUFFERSIZE;
	
	int sampleCountDown;
	
	int bufferAddress;
	
	float *soundBuffer;
	
	Channel1 ch1;
	Channel2 ch2;
	Channel3 ch3;
	Channel4 ch4;
	
	APU();
	
	void init();
	void clock(int clocks);
	
	float mixSample(float sample1, float sample2);
};