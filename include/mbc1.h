#pragma once

class MBC1
{
public:
	bool enabled;
	
	int bank1;
	int bank2;
	
	int maxBanks;
	
	int mode;
	
	char* ram;
	
	MBC1();
};