#pragma once

#include <channel.h>

class WaveChannel : public Channel
{
public:
	int sampleIndex;
	
	void triggerEvent();
	
	void updateLength(unsigned char newNRX1);
	
	void step();
	void stepTimer();
	
	void resetTimer();
	
	int getSample();
	
	int getVolumeShift();
};