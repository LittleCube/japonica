#pragma once

class PPU
{
private:
	static const int FRAMELEN = 16;
	
	static const int VRAM = 0x8000;
	static const int MAP0 = 0x9800;
	static const int MAP1 = 0x9C00;
	
	static const int LCDC = 0xFF40;
	static const int STAT = 0xFF41;
	static const int SCY = 0xFF42;
	static const int SCX = 0xFF43;
	static const int LY = 0xFF44;
	static const int LYC = 0xFF45;
	static const int WY = 0xFF4A;
	static const int WX = 0xFF4B;
	
	unsigned char lx;
	
	int numSprites;
	int spriteHeight;
	
	int prevSprite;
	
	unsigned char internalWY;
	bool windowAccessed;
	
	long totalClocks;
	
	unsigned long lastTime;
	
	Color gfx[23040];
	
	Palette BGP;
	Palette OBP[2];
	
	Sprite sprites[10];
	
public:
	PPU();
	
	void init();
	
	void clock(int clocks);
	void scanline();
	
	void setMode(int mode);
	void vbl();
	void moderateFPS();
};