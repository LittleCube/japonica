#pragma once

class Palette
{
private:
	static const Color TRUECOLORS[];
	
	int palIndex;
	
public:
	const static int OFF = 4;
	
	Palette();
	
	Palette(int palIndex);
	
	void setIndex(int palIndex);
	
	Color color(int color);
	
	static Color trueColor(int color);
};