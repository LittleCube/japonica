#pragma once

#include <envelopechannel.h>

class ToneChannel : public EnvelopeChannel
{
public:
	static const float DUTYCYCLES[4][8];
	
	int dutyIndex;
	
	void init();
	
	void step();
	void stepTimer();
};