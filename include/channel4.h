#pragma once

#include <noisechannel.h>

class Channel4 : public NoiseChannel
{
public:
	void init();
	
	float* getSamples();
};