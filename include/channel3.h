#pragma once

#include <wavechannel.h>

class Channel3 : public WaveChannel
{
public:
	void init();
	
	float* getSamples();
};