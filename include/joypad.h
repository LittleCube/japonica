#pragma once

class Joypad
{
private:
	unsigned char inputs;
	
public:
	Joypad();
	
	static const int JOYP = 0xFF00;
	
	static const int DOWN = 3;
	static const int UP = 2;
	static const int LEFT = 1;
	static const int RIGHT = 0;
	
	static const int START = 3;
	static const int SELECT = 2;
	static const int B = 1;
	static const int A = 0;
	
	unsigned char getDirections();
	void setDirectionBit(int position, int value);
	unsigned char getActions();
	void setActionBit(int position, int value);
};