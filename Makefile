####################################################
#                                                  #
#                 everything sucks                 #
#                                                  #
####################################################

INCLUDES		:= ../include ../bit/include

LIBS			:= -lbit
LIBDIRS			:= ../bit/lib

SOURCES			:= src
BUILD			:= build

export INCLUDE	:= $(foreach dir, $(INCLUDES), -I$(dir))

SRCS			:= $(foreach f, $(SOURCES), $(notdir $(wildcard $(f)/*.cpp)))

FILES			:= $(CPPFILES:.cpp=)

export OBJS		:= $(foreach f, $(SRCS), $(notdir $(f:.cpp=.o)))

LDFLAGS			:= -L$(LIBDIRS) `sdl2-config --libs` $(LIBS)

TARGET			:= gb



VPATH			 = $(foreach dir, $(SOURCES), ../$(dir)) $(foreach dir, $(INCLUDES), $(dir))

OPTFLAGS		:= -Ofast -ffast-math \
				   -ffunction-sections \
				   -fmerge-all-constants -Wl,--gc-sections \
				   -flto -fomit-frame-pointer \

DEBUGOPTFLAGS	:= -g

CPPFLAGS		:= $(INCLUDE) $(OPTFLAGS) -Wno-unused-command-line-argument -std=gnu++11



.PHONY: all dev 3ds clean realclean make-submodule-unix run



all: make-submodule-unix dev

dev: | $(BUILD)
	@$(MAKE) --no-print-directory -C $(BUILD) -f ../Makefile $(OBJS) -j
	
	@echo "linking \`$(TARGET)'..."
	
	@cd build && $(CXX) $(OBJS) -o ../$(TARGET) $(LDFLAGS)

$(BUILD):
	mkdir $@

3ds:
	$(MAKE) -f Makefile.3ds

clean:
	@echo clean ...
	
	@rm -f $(TARGET)
	
	@rm -rf $(BUILD)

realclean: clean
	@echo realclean ...
	
	@rm -rf bit/
	@rm -f submodule

bit:
	@$(MAKE) clean
	@git submodule deinit -f .
	@git submodule update --init --recursive --remote --merge
	@git submodule foreach git pull origin master
	@cp -rf bitutil-cpp/ bit/

make-submodule-unix: | bit
	@cd bit/ && make

run:
	@./$(TARGET) dmg-acid2.gb
