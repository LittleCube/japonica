#include <main.h>

PPU::PPU()
{
	init();
}

void PPU::init()
{
	lx = 0;
	
	numSprites = 0;
	spriteHeight = 0;
	
	prevSprite = -1;
	
	internalWY = 0;
	windowAccessed = false;
	
	totalClocks = 0;
	
	lastTime = Util::getTime();
	
	mmu.memory[LY] = 0;
	mmu.memory[SCX] = 0;
	mmu.memory[SCY] = 0;
	
	BGP = Palette(0xFF47);
	OBP[0] = Palette(0xFF48);
	OBP[1] = Palette(0xFF49);
}

void PPU::clock(int clocks)
{
	totalClocks += clocks;
	
	if (totalClocks >= 456)
	{
		scanline();
		
		totalClocks -= 456;
	}
}

void PPU::scanline()
{
	unsigned char ly = mmu.memory[LY];
	
	unsigned char lcdc = mmu.memory[LCDC];
	
	unsigned char wx = mmu.memory[WX];
	unsigned char wy = mmu.memory[WY];
	
	if (ly <= 143)
	{
		if (getBit(1, lcdc) == 1)
		{
			Sprite temp;
			
			numSprites = 0;
			
			for (int i = 0; (i < 40) && (numSprites < 10); i++)
			{
				// TODO: possibly only get sprite's y, this is slow
				temp.updateSprite(0xFE00 + (i * 4));
				
				spriteHeight = 8;
				
				if (getBit(2, lcdc) == 1)
				{
					spriteHeight = 16;
				}
				
				if (ly >= (temp.y - 16) && (ly < (temp.y - (16 - spriteHeight))))
				{
					sprites[numSprites++] = temp;
				}
			}
			
			for (int i = 0; i < numSprites; i++)
			{
				for (int j = i + 1; j < numSprites; j++)
				{
					if ((sprites[i].x < sprites[j].x) || (sprites[i].x == sprites[j].x))
					{
						temp = sprites[i];
						
						sprites[i] = sprites[j];
						
						sprites[j] = temp;
					}
				}
			}
		}
		
		while (lx < 160)
		{
			int currPos = lx + (ly * Drivers::G_W);
			
			if (getBit(0, lcdc) == 0)
			{
				gfx[currPos] = Palette::trueColor(0);
			}
			
			else
			{
				unsigned char x;
				unsigned char y;
				
				unsigned short map;
				
				int possiblePixel;
				
				Tile t;
				
				unsigned char tileNo = 0x00;
				
				int tempWX = wx - 7;
				
				if ((lx >= tempWX) && (ly >= wy) && (getBit(5, lcdc) == 1))
				{
					// Window
					
					x = lx - tempWX;
					y = internalWY;
					
					if (getBit(6, lcdc) == 0)
					{
						map = MAP0;
					}
					
					else
					{
						map = MAP1;
					}
					
					windowAccessed = true;
				}
				
				else
				{
					// BG
					
					x = lx + mmu.memory[SCX];
					y = ly + mmu.memory[SCY];
					
					if (getBit(3, lcdc) == 0)
					{
						map = MAP0;
					}
					
					else
					{
						map = MAP1;
					}
				}
				
				tileNo = mmu.memory[map + ((x / 8) + ((y / 8) * 32))];
				
				if (getBit(4, lcdc) == 0)
				{
					t.setIndex(VRAM + ((signed char) (tileNo) * 16) + 0x1000);
				}
				
				else
				{
					t.setIndex(VRAM + (tileNo * 16));
				}
				
				possiblePixel = t.getPixel(x % 8, y % 8);
				
				int spritePal = -1;
				
				if (numSprites > 0 && (getBit(1, lcdc) == 1))
				{
					// Sprite
					
					#define spr sprites[i]
					
					for (int i = numSprites - 1; i >= 0 && (spritePal == -1); i--)
					{
						if ((!spr.priority || (possiblePixel == 0)))
						{
							if ((lx >= (spr.x - 8)) && (lx < spr.x))
							{
								x = lx - (spr.x - 8);
								y = ly - (spr.y - 16);
								
								tileNo = spr.tileNo;
								
								if (spr.xflip)
								{
									x = abs(x - 7);
								}
								
								if (spriteHeight == 16)
								{
									if (((y >= 8) && !spr.yflip) || ((y < 8) && spr.yflip))
									{
										tileNo |= 0x01;
									}
									
									else
									{
										tileNo &= 0xFE;
									}
								}
								
								if (spr.yflip)
								{
									y = abs(y - 15);
									
									// TODO: make sure this y-flip
									//       code actually works
								}
								
								t.setIndex(VRAM + (tileNo * 16));
								
								int possibleSpritePixel = t.getPixel(x % 8, y % 8);
								
								if (possibleSpritePixel != 0)
								{
									spritePal = spr.palette;
									
									possiblePixel = possibleSpritePixel;
								}
							}
						}
					}
				}
				
				if (spritePal > -1)
				{
					gfx[currPos] = OBP[spritePal].color(possiblePixel);
				}
				
				else
				{
					gfx[currPos] = BGP.color(possiblePixel);
				}
			}
			
			lx++;
		}
		
		if (windowAccessed)
		{
			internalWY++;
			
			windowAccessed = false;
		}
		
		if (getBit(3, mmu.memory[STAT]) == 1)
		{
			cpu.intr(CPU::STAT);
		}
		
		numSprites = 0;
		
		lx = 0;
		
		if (getBit(5, mmu.memory[STAT]) == 1)
		{
			cpu.intr(CPU::STAT);
		}
	}
	
	else if (ly == 144)
	{
		vbl();
	}
	
	mmu.memory[LY] = (++ly) % 154;
	
	if (mmu.memory[LY] == mmu.memory[LYC])
	{
		setBit(2, 1, mmu.memory[STAT]);
		
		if (getBit(6, mmu.memory[STAT]) == 1)
		{
			cpu.intr(CPU::STAT);
		}
	}
	
	else
	{
		setBit(2, 0, mmu.memory[STAT]);
	}
}

void PPU::setMode(int mode)
{
	mmu.memory[STAT] &= 0xFC;
	
	mmu.memory[STAT] |= (mode & 3);
}

void PPU::vbl()
{
	internalWY = 0;
	
	cpu.intr(CPU::VBL);
	
	if (getBit(4, mmu.memory[STAT]) == 1)
	{
		cpu.intr(CPU::STAT);
	}
	
	drivers.setPixels(gfx);
	
	moderateFPS();
}

void PPU::moderateFPS()
{
	int difference = Util::getTime() - lastTime;
	
	if (difference < FRAMELEN)
	{
		Util::msleep(FRAMELEN - difference);
	}
	
	lastTime = Util::getTime();
}