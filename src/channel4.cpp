#include <main.h>

void Channel4::init()
{
	NoiseChannel::init();
	
	ADDR_NRX1 = 0xFF20;
	ADDR_NRX2 = 0xFF21;
	ADDR_NRX3 = 0xFF22;
	ADDR_NRX4 = 0xFF23;
	
	NRX1 = 0;
	NRX2 = 0;
	NRX3 = 0;
	NRX4 = 0;
}

float* Channel4::getSamples()
{
	float *samples = new float[2];
	
	if ((lengthCounter > 0) || (getBit(6, NRX4) == 0))
	{
		int dacInput = getSample() * currentVolume;
		
		float dacOutput = (dacInput / 400.0f);
		
		if (getBit(3, mmu.memory[APU::NR51]))
		{
			int volume = mmu.memory[APU::NR50] & 7;
			
			samples[0] = dacOutput * volume;
		}
		
		else
		{
			samples[0] = 0;
		}
		
		if (getBit(7, mmu.memory[APU::NR51]))
		{
			int volume = (mmu.memory[APU::NR50] >> 4) & 7;
			
			samples[1] = dacOutput * volume;
		}
		
		else
		{
			samples[1] = 0;
		}
	}
	
	else
	{
		samples[0] = 0;
		samples[1] = 0;
	}
	
	return samples;
}