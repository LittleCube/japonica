#include <main.h>

Timer::Timer()
{
	init();
}

void Timer::init()
{
	internal = 0;
	
	prevAND = 0;
}

void Timer::clock(int clocks)
{
	while (clocks > 0)
	{
		internal++;
		
		int thisAND = getBit(2, mmu.read(TAC)) & getBit(divBit(), internal);
		
		if (prevAND == 1 && thisAND == 0)
		{
			incTima();
		}
		
		prevAND = thisAND;
		
		clocks--;
	}
	
	mmu.write(DIV, subByte(1, internal));
}

int Timer::divBit()
{
	int divBit;
	
	unsigned char tac = mmu.read(TAC);
	
	unsigned char tacSelect = tac & 3;
	
	switch (tacSelect)
	{
		case 0:
		{
			divBit = 9;
			
			break;
		}
		
		case 1:
		{
			divBit = 3;
			
			break;
		}
		
		case 2:
		{
			divBit = 5;
			
			break;
		}
		
		case 3:
		{
			divBit = 7;
			
			break;
		}
	}
	
	return divBit;
}

void Timer::incTima()
{
	int tima = mmu.read(TIMA);
	
	if ((tima + 1) > 0xFF)
	{
		cpu.intr(CPU::TIMER);
		
		mmu.write(TIMA, mmu.read(TMA));
		
		cpu.t += 4;
	}
	
	else
	{
		mmu.write(TIMA, tima + 1);
	}
}