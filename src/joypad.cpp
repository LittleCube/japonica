#include <main.h>

Joypad::Joypad()
{
	inputs = 0xFF;
}

unsigned char Joypad::getDirections()
{
	return (unsigned char) subBits(0, 3, inputs);
}

void Joypad::setDirectionBit(int position, int value)
{
	if ((getBit(position, inputs) == 1) && value == 0)
	{
		cpu.intr(CPU::JOYPAD);
	}
	
	setBit(position, value, inputs);
}

unsigned char Joypad::getActions()
{
	return (unsigned char) subBits(4, 7, inputs);
}

void Joypad::setActionBit(int position, int value)
{
	int tmpPosition = position + 4;
	
	if ((getBit(tmpPosition, inputs) == 1) && value == 0)
	{
		cpu.intr(CPU::JOYPAD);
	}
	
	setBit(tmpPosition, value, inputs);
}