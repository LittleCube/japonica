#include <main.h>

#include <sys/stat.h>

MMU::MMU()
{
	init();
}

void MMU::init()
{
	memory = new unsigned char[0x10000];
	
	time_t t;
	
	srand((unsigned) time(&t));
	
	for (int i = 0; i < 0x10000; i++)
	{
		memory[i] = rand() % 0x100;
	}
	
	bios = new char[0x100];
	
	mbcType = -1;
}

unsigned char MMU::read(int addr)
{
	unsigned char value = memory[addr];
	
	readEffects(addr, value);
	
	return value;
}

void MMU::readEffects(int addr, unsigned char &value)
{
	if (useBIOS && addr < 0x100)
	{
		value = bios[addr];
	}
	
	else if (mbcType > 0)
	{
		int bankNumber = -1;
		
		bool romAccess = false;
		bool ramAccess = false;
		bool badValue = false;
		
		if (addr >= 0x0000 && addr <= 0x3FFF)
		{
			if (bigRom && (mbc1.mode == 1))
			{
				bankNumber = mbc1.bank2 << 5;
			}
			
			else
			{
				bankNumber = 0;
			}
			
			romAccess = true;
		}
		
		else if (addr >= 0x4000 && addr <= 0x7FFF)
		{
			addr &= 0x3FFF;
			
			bankNumber = mbc1.bank1;
			
			if (bigRom)
			{
				bankNumber |= (mbc1.bank2 << 5);
			}
			
			romAccess = true;
		}
		
		else if (addr >= 0xA000 && addr <= 0xBFFF)
		{
			if (mbc1.enabled)
			{
				addr &= 0x1FFF;
				
				if (bigRam && (mbc1.mode == 1))
				{
					bankNumber = mbc1.bank2;
				}
				
				else
				{
					bankNumber = 0;
				}
			}
			
			else
			{
				badValue = true;
			}
			
			ramAccess = true;
		}
		
		else if (addr >= 0xE000 && addr <= 0xFDFF)
		{
			value = memory[addr - 0x2000];
		}
		
		if (!badValue)
		{
			if (romAccess)
			{
				if (bankNumber == -1)
				{
					printf("E: bank number didn't happen (rom) :P\n");
					
					Util::quit();
				}
				
				value = fullrom[(bankNumber << 14) | addr];
			}
			
			else if (ramAccess)
			{
				if (bankNumber == -1)
				{
					printf("E: bank number didn't happen (ram) :P\n");
					
					Util::quit();
				}
				
				value = mbc1.ram[(bankNumber << 13) | addr];
			}
		}
		
		else
		{
			value = 0xFF;
		}
	}
}

void MMU::writeBit(int position, int value, int addr)
{
	unsigned char byte = read(addr);
	
	setBit(position, value, byte);
	
	write(addr, byte);
}

void MMU::write(int addr, unsigned char value)
{
	if (writeEffects(addr, value))
	{
		memory[addr] = value;
	}
}

bool MMU::writeEffects(int addr, unsigned char &value)
{
	bool validWrite = true;
	
	if (addr >= 0x0000 && addr <= 0x7FFF)
	{
		validWrite = false;
		
		if (addr >= 0x0000 && addr <= 0x1FFF)
		{
			if ((value & 0xF) == 0xA)
			{
				mbc1.enabled = true;
			}
			
			else
			{
				mbc1.enabled = false;
			}
		}
		
		else if (addr >= 0x2000 && addr <= 0x3FFF)
		{
			mbc1.bank1 = value & (mbc1.maxBanks);
			
			if (mbc1.bank1 == 0)
			{
				mbc1.bank1 = 1;
			}
		}
		
		else if (addr >= 0x4000 && addr <= 0x5FFF)
		{
			mbc1.bank2 = value & 0x03;
		}
		
		else if (addr >= 0x6000 && addr <= 0x7FFF)
		{
			mbc1.mode = value & 0x01;
		}
	}
	
	else if (addr >= 0xA000 && addr <= 0xBFFF)
	{
		if (mbcType > 0)
		{
			if (mbc1.enabled)
			{
				addr &= 0x1FFF;
				
				if (bigRam && (mbc1.mode == 1))
				{
					mbc1.ram[(mbc1.bank2 << 13) | addr] = value;
				}
				
				else
				{
					mbc1.ram[addr] = value;
				}
				
				if (battery)
				{
					save.write(mbc1.ram, ramLen);
					save.seekp(save.beg);
				}
			}
		}
	}
	
	else
	{
		switch (addr)
		{
			// TODO: reset DIV when written
			
			case 0xFF00:
			{
				if ((getBit(4, value) == 1) && (getBit(5, value) == 1))
				{
					setBits(0, 3, 0xFF, value);
				}
				
				else
				{
					if (getBit(4, value) == 0)
					{
						value |= joypad.getDirections();
					}
					
					if (getBit(5, value) == 0)
					{
						value |= joypad.getActions();
					}
				}
				
				value |= 0xC0;
				
				break;
			}
			
			case 0xFF11: // NR11
			{
				apu.ch1.updateLength(value);
				
				break;
			}
			
			case 0xFF14: // NR14
			{
				if (getBit(7, value))
				{
					apu.ch1.triggerEvent();
				}
				
				break;
			}
			
			case 0xFF16: // NR21
			{
				apu.ch2.updateLength(value);
				
				break;
			}
			
			case 0xFF19: // NR24
			{
				if (getBit(7, value))
				{
					apu.ch2.triggerEvent();
				}
				
				break;
			}
			
			case 0xFF1B: // NR31
			{
				apu.ch3.updateLength(value);
				
				break;
			}
			
			case 0xFF1E: // NR34
			{
				if (getBit(7, value))
				{
					apu.ch3.triggerEvent();
				}
				
				break;
			}
			
			case 0xFF20: // NR41
			{
				apu.ch4.updateLength(value);
				
				break;
			}
			
			case 0xFF23: // NR44
			{
				if (getBit(7, value))
				{
					apu.ch4.triggerEvent();
				}
				
				break;
			}
			
			case 0xFF46:
			{
				dma(value);
				
				break;
			}
			
			case 0xFF50:
			{
				if (value != 0)
				{
					useBIOS = false;
				}
				
				break;
			}
		}
	}
	
	return validWrite;
}

void MMU::dma(int addr)
{
	addr *= 0x100;
	
	for (int i = 0x00; i < 0xA0; i++)
	{
		memory[0xFE00 + i] = memory[addr + i];
	}
	
	cpu.t += 644;
}

bool MMU::loadROM(string path, bool isBios)
{
	bool success = true;
	
	ifstream romfile(path, ios::in | ios::binary);
	
	if (!romfile.good())
	{
		success = false;
	}
	
	else
	{
		if (isBios)
		{
			romfile.read(bios, BIOSLEN);
		}
		
		else
		{
			struct stat sb{};
			
			if (!stat(path.c_str(), &sb))
			{
				fullRomLen = sb.st_size;
			}
			
			else
			{
				printf("E: couldn't get filesize\n");
				Util::quit();
			}
			
			fullrom = new char[fullRomLen];
			romfile.read(fullrom, fullRomLen);
			
			if (fullRomLen >= 0x100000)
			{
				bigRom = true;
			}
			
			for (int i = 0; i < ROMAREALEN; i++)
			{
				memory[i] = fullrom[i];
			}
			
			battery = false;
			
			switch (fullrom[0x147]) // Mapper Type
			{
				case 0x01:
				case 0x02:
				case 0x03:
				{
					mbcType = 1;
					
					if (fullrom[0x147] == 0x03)
					{
						battery = true;
					}
					
					break;
				}
				
				default:
				{
					mbcType = 0;
					
					break;
				}
			}
			
			switch (fullrom[0x148]) // ROM Size
			{
				case 0x01:
				{
					mbc1.maxBanks = 3;
					
					break;
				}
				
				case 0x02:
				{
					mbc1.maxBanks = 7;
					
					break;
				}
				
				case 0x03:
				{
					mbc1.maxBanks = 15;
					
					break;
				}
				
				case 0x04:
				{
					mbc1.maxBanks = 31;
					
					break;
				}
				
				case 0x05:
				{
					mbc1.maxBanks = 63;
					
					break;
				}
				
				case 0x06:
				{
					mbc1.maxBanks = 127;
					
					break;
				}
				
				default:
				{
					mbc1.maxBanks = 1;
					
					break;
				}
			}
			
			switch (fullrom[0x149]) // RAM Size
			{
				case 0x02:
				{
					ramLen = 0x2000;
					
					break;
				}
				
				bigRam = true;
				
				case 0x03:
				{
					ramLen = 0x2000 * 4;
					
					break;
				}
				
				case 0x04:
				{
					ramLen = 0x2000 * 16;
					
					break;
				}
				
				case 0x05:
				{
					ramLen = 0x2000 * 8;
					
					break;
				}
				
				default:
				{
					ramLen = 0;
					
					break;
				}
			}
			
			mbc1.ram = new char[ramLen];
			
			if (battery)
			{
				string savePath = path.substr(0, path.find_last_of("."));
				savePath += ".srm";
				// ^^^ if there's no dot it'll just do nothing and add the '.srm'
				
				ifstream prevSave(savePath, ios::in | ios::binary);
				
				if (prevSave.good())
				{
					prevSave.read(mbc1.ram, ramLen);
				}
				
				save.open(savePath, ios::out | ios::binary);
				
				save.write(mbc1.ram, ramLen);
				save.seekp(save.beg);
			}
		}
	}
	
	return success;
}