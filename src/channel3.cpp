#include <main.h>

void Channel3::init()
{
	WaveChannel::init();
	
	ADDR_NRX0 = 0xFF1A;
	ADDR_NRX1 = 0xFF1B;
	ADDR_NRX2 = 0xFF1C;
	ADDR_NRX3 = 0xFF1D;
	ADDR_NRX4 = 0xFF1E;
	
	NRX0 = 0;
	NRX1 = 0;
	NRX2 = 0;
	NRX3 = 0;
	NRX4 = 0;
}

float* Channel3::getSamples()
{
	float *samples = new float[2];
	
	if (((lengthCounter > 0) || (getBit(6, NRX4) == 0)) && ((NRX0 >> 7) == 1))
	{
		int dacInput = getSample();
		
		float dacOutput = (dacInput / 250.0f);
		
		if (getBit(2, mmu.memory[APU::NR51]))
		{
			int volume = mmu.memory[APU::NR50] & 7;
			
			samples[0] = dacOutput * volume;
		}
		
		else
		{
			samples[0] = 0;
		}
		
		if (getBit(6, mmu.memory[APU::NR51]))
		{
			int volume = (mmu.memory[APU::NR50] >> 4) & 7;
			
			samples[1] = dacOutput * volume;
		}
		
		else
		{
			samples[1] = 0;
		}
	}
	
	else
	{
		samples[0] = 0;
		samples[1] = 0;
	}
	
	return samples;
}