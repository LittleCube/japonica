#include <main.h>

void Channel1::init()
{
	ToneChannel::init();
	
	sweepEnabled = false;
	
	shadowFrequency = 0;
	sweepTimer = 0;
	
	ADDR_NRX0 = 0xFF10;
	ADDR_NRX1 = 0xFF11;
	ADDR_NRX2 = 0xFF12;
	ADDR_NRX3 = 0xFF13;
	ADDR_NRX4 = 0xFF14;
	
	NRX0 = 0;
	NRX1 = 0;
	NRX2 = 0;
	NRX3 = 0;
	NRX4 = 0;
}

void Channel1::triggerEvent()
{
	EnvelopeChannel::triggerEvent();
	
	shadowFrequency = NRX3;
	shadowFrequency |= (NRX4 & 7) << 8;
	
	resetSweepTimer();
	
	if (getSweepShift() != 0 || getSweepPeriod() != 0)
	{
		sweepEnabled = true;
		
		if (getSweepPeriod() != 0)
		{
			overflowCheck(calcSweepFreq());
		}
	}
	
	else
	{
		sweepEnabled = false;
	}
}

void Channel1::step()
{
	ToneChannel::stepTimer();
	
	stepFrameSequencer();
}

void Channel1::stepFrameSequencer()
{
	if (--frameSequencerTimer == 0)
	{
		frameSequencerTimer = 8192;
		
		performFrameStep();
		
		frameSequencerStep++;
		frameSequencerStep &= 7;
	}
}

void Channel1::performFrameStep()
{
	ToneChannel::performFrameStep();
	
	switch (frameSequencerStep)
	{
		case 2:
		case 6:
		{
			if (--sweepTimer == 0)
			{
				int sweepPeriod = getSweepPeriod();
				
				resetSweepTimer();
				
				if (sweepEnabled && sweepPeriod != 0)
				{
					unsigned short newFrequency = calcSweepFreq();
					
					overflowCheck(newFrequency);
					
					if (newFrequency < 2048 && getSweepShift() != 0)
					{
						shadowFrequency = newFrequency;
						
						NRX3 = newFrequency & 0xFF;
						
						NRX4 &= 0xF8;
						NRX4 |= (newFrequency >> 8) & 7;
						
						overflowCheck(calcSweepFreq());
					}
				}
			}
			
			break;
		}
	}
}

void Channel1::resetSweepTimer()
{
	unsigned char sweepPeriod = getSweepPeriod();
	
	if (sweepPeriod != 0)
	{
		sweepTimer = sweepPeriod;
	}
	
	else
	{
		sweepTimer = 8;
	}
}

unsigned char Channel1::getSweepPeriod()
{
	return (NRX0 >> 4) & 7;
}

unsigned short Channel1::calcSweepFreq()
{
	unsigned short newFrequency;
	
	int difference = shadowFrequency >> getSweepShift();
	
	if (getBit(3, NRX0))
	{
		newFrequency = shadowFrequency - difference;
	}
	
	else
	{
		newFrequency = shadowFrequency + difference;
	}
	
	return newFrequency;
}

unsigned char Channel1::getSweepShift()
{
	return (NRX0 & 7);
}

void Channel1::overflowCheck(unsigned short freq)
{
	if (freq > 2047)
	{
		sweepEnabled = false;
	}
}

float* Channel1::getSamples()
{
	int dutySelect = (NRX1 >> 6) & 3;
	
	float *samples = new float[2];
	
	if ((lengthCounter > 0) || (getBit(6, NRX4) == 0))
	{
		int dacInput = DUTYCYCLES[dutySelect][dutyIndex] * currentVolume;
		
		float dacOutput = (dacInput / 100.0f);
		
		if (getBit(0, mmu.memory[APU::NR51]))
		{
			int volume = mmu.memory[APU::NR50] & 7;
			
			samples[0] = dacOutput * volume;
		}
		
		else
		{
			samples[0] = 0;
		}
		
		if (getBit(4, mmu.memory[APU::NR51]))
		{
			int volume = (mmu.memory[APU::NR50] >> 4) & 7;
			
			samples[1] = dacOutput * volume;
		}
		
		else
		{
			samples[1] = 0;
		}
	}
	
	else
	{
		samples[0] = 0;
		samples[1] = 0;
	}
	
	return samples;
}