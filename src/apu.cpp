#include <main.h>

const int APU::NR50 = 0xFF24;
const int APU::NR51 = 0xFF25;
const int APU::NR52 = 0xFF26;

const int APU::BUFFERSIZE = 1024;

const int APU::MAXCOUNTDOWN = 95;

APU::APU()
{
	init();
}

void APU::init()
{
	soundBuffer = new float[APU::BUFFERSIZE];
	
	sampleCountDown = MAXCOUNTDOWN;
	
	bufferAddress = 0;
	
	ch1.init();
	ch2.init();
	ch3.init();
	ch4.init();
}

void APU::clock(int clocks)
{
	while (clocks-- > 0)
	{
		if (!getBit(7, mmu.memory[NR52]))
		{
			return;
		}
		
		ch1.step();
		ch2.step();
		ch3.step();
		ch4.step();
		
		if (--sampleCountDown == 0)
		{
			sampleCountDown = MAXCOUNTDOWN;
			
			float *ch1Samples = ch1.getSamples();
			float *ch2Samples = ch2.getSamples();
			float *ch3Samples = ch3.getSamples();
			float *ch4Samples = ch4.getSamples();
			
			float *finalSamples = new float[2];
			
			for (int i = 0; i < 2; i++)
			{
				finalSamples[i] = mixSample(ch1Samples[i], ch2Samples[i]);
				finalSamples[i] = mixSample(finalSamples[i], ch3Samples[i]);
				finalSamples[i] = mixSample(finalSamples[i], ch4Samples[i]);
			}
			
			soundBuffer[bufferAddress] = finalSamples[0];
			soundBuffer[bufferAddress + 1] = finalSamples[1];
			
			bufferAddress += 2;
			
			if (bufferAddress == APU::BUFFERSIZE)
			{
				drivers.fillAudio(soundBuffer, APU::BUFFERSIZE);
				
				bufferAddress = 0;
			}
		}
	}
}

float APU::mixSample(float sample1, float sample2)
{
	float finalSample = (sample1 + sample2) / 2;
	
	return finalSample;
}