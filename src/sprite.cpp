#include <main.h>

Sprite::Sprite()
{
	y = 0x00;
	x = 0x00;
	
	tileNo = 0x00;
	
	priority = -1;
	
	yflip = -1;
	xflip = -1;
	
	palette = -1;
}

Sprite::Sprite(int offset)
{
	updateSprite(offset);
}

void Sprite::updateSprite(int offset)
{
	y = mmu.read(offset);
	x = mmu.read(offset + 1);
	
	tileNo = mmu.read(offset + 2);
	
	unsigned char attributes = mmu.read(offset + 3);
	
	priority = getBit(7, attributes);
	
	yflip = getBit(6, attributes);
	xflip = getBit(5, attributes);
	
	palette = getBit(4, attributes);
}