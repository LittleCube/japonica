#include <main.h>

Channel::Channel()
{
	init();
}

void Channel::init()
{
	lengthCounter = 0;
	
	timer = 0;
	frameSequencerTimer = 8192;
	frameSequencerStep = 0;
}

void Channel::triggerEvent()
{
	resetTimer();
}

void Channel::stepFrameSequencer()
{
	if (--frameSequencerTimer == 0)
	{
		frameSequencerTimer = 8192;
		
		performFrameStep();
		
		frameSequencerStep++;
		frameSequencerStep &= 7;
	}
}

void Channel::performFrameStep()
{
	switch (frameSequencerStep)
	{
		case 0:
		case 2:
		case 4:
		case 6:
		{
			decLength();
			
			break;
		}
	}
}

void Channel::updateLength(unsigned char newNRX1)
{
	if (lengthCounter == 0)
	{
		lengthCounter = 64 - (newNRX1 & 0x3F);
	}
}

void Channel::decLength()
{
	if (getBit(6, NRX4) && (lengthCounter > 0))
	{
		lengthCounter--;
	}
}

void Channel::resetTimer()
{
	int freq = ((NRX4 & 7) << 8) | NRX3;
	
	timer = (2048 - freq) * 4;
}