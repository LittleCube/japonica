#include <main.h>

EnvelopeChannel::EnvelopeChannel()
{
	init();
}

void EnvelopeChannel::init()
{
	Channel::init();
	
	currentVolume = 0;
	periodTimer = 0;
}

void EnvelopeChannel::triggerEvent()
{
	Channel::triggerEvent();
	
	periodTimer = NRX2 & 7;
	currentVolume = (NRX2 >> 4) & 0xF;
}

void EnvelopeChannel::stepFrameSequencer()
{
	if (--frameSequencerTimer == 0)
	{
		frameSequencerTimer = 8192;
		
		performFrameStep();
		
		frameSequencerStep++;
		frameSequencerStep &= 7;
	}
}

void EnvelopeChannel::performFrameStep()
{
	Channel::performFrameStep();
	
	switch (frameSequencerStep)
	{
		case 7:
		{
			int period = NRX2 & 7;
			
			if (period != 0 && --periodTimer == 0)
			{
				periodTimer = period;
				
				if ((currentVolume < 0xF) && getBit(3, NRX2))
				{
					currentVolume++;
				}
				
				else if ((currentVolume > 0x0) && !getBit(3, NRX2))
				{
					currentVolume--;
				}
			}
			
			break;
		}
	}
}