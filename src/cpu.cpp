#include <main.h>
#include <cpu.h>

#define FUNC(ptr) bind(ptr, this)
#define ADDF(addr, ptr) cpufunc[addr] = FUNC(ptr)

CPU::CPU()
{
	initMMU();
	
	init();
}

void CPU::initMMU()
{
	mmu = MMU();
}

void CPU::init()
{
	opcode = 0;
	
	t = 0;
	
	y = 0;
	z = 0;
	p = 0;
	q = 0;
	
	d = 0;
	n = 0;
	nn = 0;
	
	pc = 0;
	sp = 0;
	
	r = new unsigned char[9];
	
	ime = false;
	
	cpuStop = false;
	cpuHalt = false;
	
	eiBool = false;
	
	for (int i = 0; i < 9; i++)
	{
		r[i] = 0;
	}
	
	// begin nightmarish func pointer code
	for (int i = 0; i < 512; i++)
	{
		switch (i)
		{
			case 0x01:
			case 0x11:
			case 0x21:
			case 0x31:
			{
				ADDF(i, &CPU::ld_rp);
				break;
			}
			
			case 0x20:
			case 0x28:
			case 0x30:
			case 0x38:
			{
				ADDF(i, &CPU::jr_cc);
				break;
			}
			
			case 0x09:
			case 0x19:
			case 0x29:
			case 0x39:
			{
				ADDF(i, &CPU::add_hl);
				break;
			}
			
			case 0x02:
			case 0x12:
			{
				ADDF(i, &CPU::ld_rp_a);
				break;
			}
			
			case 0x0A:
			case 0x1A:
			{
				ADDF(i, &CPU::ld_a_rp);
				break;
			}
			
			case 0x22:
			case 0x32:
			{
				ADDF(i, &CPU::ld_hl_a);
				break;
			}
			
			case 0x03:
			case 0x13:
			case 0x23:
			case 0x33:
			{
				ADDF(i, &CPU::inc_rp);
				break;
			}
			
			case 0x2A:
			case 0x3A:
			{
				ADDF(i, &CPU::ld_a_hl);
				break;
			}
			
			case 0x0B:
			case 0x1B:
			case 0x2B:
			case 0x3B:
			{
				ADDF(i, &CPU::dec_rp);
				break;
			}
			
			case 0x04:
			case 0x0C:
			case 0x14:
			case 0x1C:
			case 0x24:
			case 0x2C:
			case 0x34:
			case 0x3C:
			{
				ADDF(i, &CPU::inc_r);
				break;
			}
			
			case 0x05:
			case 0x0D:
			case 0x15:
			case 0x1D:
			case 0x25:
			case 0x2D:
			case 0x35:
			case 0x3D:
			{
				ADDF(i, &CPU::dec_r);
				break;
			}
			
			case 0x06:
			case 0x0E:
			case 0x16:
			case 0x1E:
			case 0x26:
			case 0x2E:
			case 0x36:
			case 0x3E:
			{
				ADDF(i, &CPU::ld_r_n);
				break;
			}
			
			case 0x40 ... 0x7F:
			{
				ADDF(i, &CPU::ld_r_r);
				break;
			}
			
			case 0x80 ... 0xBF:
			{
				ADDF(i, &CPU::alu_r);
				break;
			}
			
			case 0xC0:
			case 0xC8:
			case 0xD0:
			case 0xD8:
			{
				ADDF(i, &CPU::ret_cc);
				break;
			}
			
			case 0xC1:
			case 0xD1:
			case 0xE1:
			case 0xF1:
			{
				ADDF(i, &CPU::pop_rp2);
				break;
			}
			
			case 0xC2:
			case 0xCA:
			case 0xD2:
			case 0xDA:
			{
				ADDF(i, &CPU::jp_cc_nn);
				break;
			}
			
			case 0xC4:
			case 0xCC:
			case 0xD4:
			case 0xDC:
			{
				ADDF(i, &CPU::call_cc_nn);
				break;
			}
			
			case 0xC5:
			case 0xD5:
			case 0xE5:
			case 0xF5:
			{
				ADDF(i, &CPU::push_rp2);
				break;
			}
			
			case 0xC6:
			case 0xCE:
			case 0xD6:
			case 0xDE:
			case 0xE6:
			case 0xEE:
			case 0xF6:
			case 0xFE:
			{
				ADDF(i, &CPU::alu_n);
				break;
			}
			
			case 0xC7:
			case 0xCF:
			case 0xD7:
			case 0xDF:
			case 0xE7:
			case 0xEF:
			case 0xF7:
			case 0xFF:
			{
				ADDF(i, &CPU::rst);
				break;
			}
			
			case 0x100 ... 0x13F:
			{
				ADDF(i, &CPU::rot_r);
				break;
			}
			
			case 0x140 ... 0x17F:
			{
				ADDF(i, &CPU::bit);
				break;
			}
			
			case 0x180 ... 0x1BF:
			{
				ADDF(i, &CPU::res);
				break;
			}
			
			case 0x1C0 ... 0x1FF:
			{
				ADDF(i, &CPU::set);
				break;
			}
			
			default:
			{
				ADDF(i, &CPU::error);
				break;
			}
		}
	}
	
	ADDF(0x00, &CPU::nop);
	ADDF(0x07, &CPU::rlca);
	ADDF(0x08, &CPU::ld_nn_sp);
	ADDF(0x0F, &CPU::rrca);
	ADDF(0x10, &CPU::stop);
	ADDF(0x17, &CPU::rla);
	ADDF(0x18, &CPU::jr);
	ADDF(0x1F, &CPU::rra);
	ADDF(0x27, &CPU::daa);
	ADDF(0x2F, &CPU::cpl);
	ADDF(0x37, &CPU::scf);
	ADDF(0x3F, &CPU::ccf);
	ADDF(0x76, &CPU::halt);
	ADDF(0xC3, &CPU::jp_nn);
	ADDF(0xC9, &CPU::_ret);
	ADDF(0xCD, &CPU::call_nn);
	ADDF(0xD9, &CPU::reti);
	ADDF(0xE0, &CPU::ld_io_a);
	ADDF(0xE2, &CPU::ld_ioc_a);
	ADDF(0xE8, &CPU::add_sp_d);
	ADDF(0xE9, &CPU::jp_hl);
	ADDF(0xEA, &CPU::ld_nn_a);
	ADDF(0xF0, &CPU::ld_a_io);
	ADDF(0xF2, &CPU::ld_a_ioc);
	ADDF(0xF3, &CPU::di);
	ADDF(0xF8, &CPU::ld_hl_spd);
	ADDF(0xF9, &CPU::ld_sp_hl);
	ADDF(0xFA, &CPU::ld_a_nn);
	ADDF(0xFB, &CPU::ei);
}

void CPU::startBIOS()
{
	if (useBIOS && mmu.loadROM("./bios.gb", true))
	{
		printf("N: BIOS found\n");
	}
	
	else
	{
		printf("N: BIOS not used\n");
		
		useBIOS = false;
		
		r[A] = 0x01;
		r[B] = 0x00;
		r[C] = 0x13;
		r[D] = 0x00;
		r[E] = 0xD8;
		r[H] = 0x01;
		r[L] = 0x4D;
		r[F] = 0xB0;
		
		pc = 0x100;
		
		sp = 0xFFFE;
		
		mmu.write(0xFF05, 0x00);
		mmu.write(0xFF06, 0x00);
		mmu.write(0xFF07, 0x00);
		mmu.write(0xFF0F, 0x00);
		mmu.write(0xFF10, 0x80);
		mmu.write(0xFF11, 0xBF);
		mmu.write(0xFF12, 0xF3);
		mmu.write(0xFF14, 0xBF);
		mmu.write(0xFF16, 0x3F);
		mmu.write(0xFF17, 0x00);
		mmu.write(0xFF19, 0xBF);
		mmu.write(0xFF1A, 0x7F);
		mmu.write(0xFF1B, 0xFF);
		mmu.write(0xFF1C, 0x9F);
		mmu.write(0xFF1E, 0xBF);
		mmu.write(0xFF20, 0xFF);
		mmu.write(0xFF21, 0x00);
		mmu.write(0xFF22, 0x00);
		mmu.write(0xFF23, 0xBF);
		mmu.write(0xFF24, 0x77);
		mmu.write(0xFF25, 0xF3);
		mmu.write(0xFF26, 0xF1);
		mmu.write(0xFF40, 0x91);
		mmu.write(0xFF42, 0x00);
		mmu.write(0xFF43, 0x00);
		mmu.write(0xFF45, 0x00);
		mmu.write(0xFF47, 0xFC);
		mmu.write(0xFF48, 0xFF);
		mmu.write(0xFF49, 0xFF);
		mmu.write(0xFF4A, 0x00);
		mmu.write(0xFF4B, 0x00);
		mmu.write(0xFFFF, 0x00);
		
		for (int i = 0; i < 0x2000; i++)
		{
			mmu.write(0x8000 + i, 0x00);
		}
	}
}

void CPU::intr(int intr)
{
	mmu.writeBit(intr, 1, IF);
}



void CPU::cycle()
{
	t = 0;
	
	unsigned char if_reg = mmu.read(IF);
	unsigned char ie_reg = mmu.read(IE);
	
	if (ime)
	{
		for (int i = 0; i < 5; i++)
		{
			if ((((if_reg >> i) & 1) == 1) && (((ie_reg >> i) & 1) == 1))
			{
				call(0x40 + (8 * i));
				
				cpuHalt = false;
				
				ime = false;
				
				mmu.writeBit(i, 0, IF);
				
				t += 20;
			}
		}
	}
	
	if (!cpuHalt)
	{
		if (eiBool)
		{
			ime = true;
			
			eiBool = false;
		}
		
		opcode = mmu.read(pc);
		
		d = mmu.read(pc + 1);
		n = mmu.read(pc + 1);
		
		nn = (mmu.read(pc + 2) << 8) | mmu.read(pc + 1);
		
		if (opcode != 0xCB)
		{
			splitOpcode(opcode);
			cpufunc[opcode]();
		}
		
		else
		{
			splitOpcode(n);
			cpufunc[n + 0x100]();
		}
	}
	
	else
	{
		for (int i = 0; i < 5; i++)
		{
			if ((((if_reg >> i) & 1) == 1) && (((ie_reg >> i) & 1) == 1))
			{
				cpuHalt = false;
				
				break;
			}
		}
		
		t += 4;
	}
	
	totalT += t;
}



void CPU::nop()					// NOP (0x00)
{
	t += 4;
	pc += 1;
}

void CPU::ld_nn_sp()			// LD (nn), SP
{
	mmu.write(nn, sp & 0xFF);
	mmu.write(nn + 1, sp >> 8);
	
	if (nn == rp(HL))
	{
		updateHLMEM();
	}
	
	t += 20;
	pc += 3;
}

void CPU::stop()				// STOP (0x10)
{
	printf("W: STOP called at %04X\n", pc);
	
	// TODO: get rid of all this and break when input detected
	cpuStop = true;
	
	t += 4;
	pc += 2;
}

void CPU::jr()					// JR d
{
	pc += 2;
	
	pc += d;
	
	t += 12;
}

void CPU::jr_cc()				// JR cc[y - 4], d
{
	pc += 2;
	
	if (cc(y - 4) == 0)
	{
		t += 8;
	}
	
	else
	{
		pc += d;
		
		t += 12;
	}
}

void CPU::ld_rp()				// LD rp[p], nn
{
	rp(p, nn);
	
	t += 12;
	pc += 3;
}

void CPU::add_hl()				// ADD HL, rp[p]
{
	flags(F_HC | F_CA, 8, rp(HL), rp(p));
	
	rp(HL, rp(HL) + rp(p));
	
	t += 8;
	pc += 1;
}

								// these next four functions are for eight opcodes
void CPU::ld_rp_a()				// LD (rp[p]), A; if p < 2 then memory[rp[p]] gets loaded into. otherwise, we have to load into either memory[rp[HL]++] (p == 2) or memory[rp[HL]--] (p == 3)
{
	mmu.write(rp(p), r[A]);
	
	t += 8;
	pc += 1;
}

void CPU::ld_hl_a()				// see ^^^; if HL is being accessed, we must post-increment/decrement it
{
	mmu.write(rp(HL), r[A]);
	
	if (p == 2)
	{
		rp(HL, rp(HL) + 1);
	}
	
	else if (p == 3)
	{
		rp(HL, rp(HL) - 1);
	}
	
	t += 8;
	pc += 1;
}

void CPU::ld_a_rp()				// LD A, (rp[p]) (same thing, but other way around)
{
	r[A] = mmu.read(rp(p));
	
	t += 8;
	pc += 1;
}

void CPU::ld_a_hl()				// see ^^^; if HL is being accessed, we must post-increment/decrement it
{
	r[A] = mmu.read(rp(HL));
	
	if (p == 2)
	{
		rp(HL, rp(HL) + 1);
	}
	
	else if (p == 3)
	{
		rp(HL, rp(HL) - 1);
	}
	
	t += 8;
	pc += 1;
}

void CPU::inc_rp()				// INC rp[p]
{
	rp(p, rp(p) + 1);
	
	t += 8;
	pc += 1;
}

void CPU::dec_rp()				// DEC rp[p]
{
	rp(p, rp(p) - 1);
	
	t += 8;
	pc += 1;
}

void CPU::inc_r()				// INC r[y]
{
	flags(F_Z | F_HC, 0, r[y], 1);
	
	if (y == R_HL)
	{
		mmu.write(rp(HL), mmu.read(rp(HL)) + 1);
		
		updateHLMEM();
		
		t += 8;
	}
	
	else if (y == H || y == L)
	{
		r[y]++;
		
		updateHLMEM();
	}
	
	else
	{
		r[y]++;
	}
	
	t += 4;
	pc += 1;
}

void CPU::dec_r()				// DEC r[y]
{
	flags(F_Z | F_HC, 1, r[y], 1);
	
	if (y == R_HL)
	{
		mmu.write(rp(HL), mmu.read(rp(HL)) - 1);
		
		updateHLMEM();
		
		t += 8;
	}
	
	else if (y == H || y == L)
	{
		r[y]--;
		
		updateHLMEM();
	}
	
	else
	{
		r[y]--;
	}
	
	t += 4;
	pc += 1;
}

void CPU::ld_r_n()				// LD r[y], n
{
	r[y] = n;
	
	if (y == R_HL)
	{
		mmu.write(rp(HL), n);
		
		updateHLMEM();
		
		t += 4;
	}
	
	else if (y == H || y == L)
	{
		updateHLMEM();
	}
	
	t += 8;
	pc += 2;
}

void CPU::rlca()				// RLCA
{
	int prevBit7 = getBit(7, r[A]);
	
	cc(Z, 0);
	cc(NE, 0);
	cc(HC, 0);
	
	r[A] <<= 1;
	
	cc(CA, prevBit7);
	setBit(0, prevBit7, r[A]);
	
	t += 4;
	pc += 1;
}

void CPU::rrca()				// RRCA
{
	int prevBit0 = getBit(0, r[A]);
	
	cc(Z, 0);
	cc(NE, 0);
	cc(HC, 0);
	
	r[A] >>= 1;
	
	cc(CA, prevBit0);
	setBit(7, prevBit0, r[A]);
	
	t += 4;
	pc += 1;
}

void CPU::rla()					// RLA
{
	cc(Z, 0);
	cc(NE, 0);
	cc(HC, 0);
	
	int prevBit7 = getBit(7, r[A]);
	
	r[A] <<= 1;
	setBit(0, cc(CA), r[A]);
	
	cc(CA, prevBit7);
	
	t += 4;
	pc += 1;
}

void CPU::rra()					// RRA
{
	cc(Z, 0);
	cc(NE, 0);
	cc(HC, 0);
	
	int prevBit0 = getBit(0, r[A]);
	
	r[A] >>= 1;
	setBit(7, cc(CA), r[A]);
	
	cc(CA, prevBit0);
	
	t += 4;
	pc += 1;
}

void CPU::daa()					// DAA
{
	if (cc(NE) == 0)
	{
		if (cc(CA) == 1 || r[A] > 0x99)
		{
			r[A] += 0x60;
			cc(CA, 1);
		}
		
		if (cc(HC) == 1 || (r[A] & 0x0F) > 0x09)
		{
			r[A] += 0x6;
		}
	}
	
	else
	{
		if (cc(CA) == 1)
		{
			r[A] -= 0x60;
		}
		
		if (cc(HC) == 1)
		{
			r[A] -= 0x6;
		}
	}
	
	cc(Z, r[A] == 0 ? 1 : 0);
	cc(HC, 0);
	
	t += 4;
	pc += 1;
}

void CPU::cpl()					// CPL
{
	r[A] = ~r[A];
	
	cc(NE, 1);
	cc(HC, 1);
	
	t += 4;
	pc += 1;
}

void CPU::scf()					// SCF
{
	cc(NE, 0);
	cc(HC, 0);
	cc(CA, 1);
	
	t += 4;
	pc += 1;
}

void CPU::ccf()					// CCF
{
	cc(NE, 0);
	cc(HC, 0);
	
	cc(CA, cc(CA) ^ 1);
	
	t += 4;
	pc += 1;
}

void CPU::ld_r_r()				// LD r[y], r[z]
{
	if (y == R_HL)
	{
		mmu.write(rp(HL), r[z]);
		
		updateHLMEM();
	}
	
	else if (y == H || y == L)
	{
		r[y] = r[z];
		
		updateHLMEM();
	}
	
	else
	{
		r[y] = r[z];
	}
	
	if (y == R_HL || z == R_HL)
	{
		t += 4;
	}
	
	t += 4;
	pc += 1;
}

void CPU::halt()				// HALT
{
	if (ime)
	{
		cpuHalt = true;
	}
	
	else
	{
		if ((mmu.read(IE) & mmu.read(IF) & 0x1F) == 0)
		{
			cpuHalt = true;
		}
	}
	
	t += 4;
	pc += 1;
}

void CPU::alu_r()				// alu[y] r[z]
{
	alu(y, r[z]);
	
	t += 4;
	pc += 1;
}

void CPU::ret_cc()				// RET cc[y]
{
	if (cc(y) == 0)
	{
		t += 8;
		pc += 1;
	}
	
	else
	{
		ret();
		t += 20;
	}
}

void CPU::ld_io_a()				// LD (0xFF00 + n), A
{
	mmu.write(0xFF00 + n, r[A]);
	
	t += 12;
	pc += 2;
}

void CPU::add_sp_d()			// ADD SP, d
{
	cc(Z, 0);
	flags(F_HC | F_CA, 9, sp, d);
	
	sp += d;
	
	t += 16;
	pc += 2;
}

void CPU::ld_a_io()				// LD A, (0xFF00 + n)
{
	r[A] = mmu.read(0xFF00 + n);
	
	t += 12;
	pc += 2;
}

void CPU::ld_hl_spd()			// LD HL, SP + d
{
	int prevSP = sp;
	
	rp(HL, rp(SP) + d);
	
	cc(Z, 0);
	flags(F_HC | F_CA, 9, prevSP, d);
	
	t += 12;
	pc += 2;
}

void CPU::pop_rp2()				// POP rp2[p]
{
	rp2(p, pop());
	
	r[F] &= 0xF0;
	
	t += 12;
	pc += 1;
}

void CPU::_ret()				// RET
{
	ret();
	
	t += 16;
}

void CPU::reti()				// RETI
{
	ime = true;
	
	ret();
	
	t += 16;
}

void CPU::jp_hl()				// JP HL
{
	pc = rp(HL);
	
	t += 4;
}

void CPU::ld_sp_hl()			// LD SP, HL
{
	sp = rp(HL);
	
	t += 8;
	pc += 1;
}

void CPU::jp_cc_nn()			// JP cc[y], nn
{
	if (cc(y) == 0)
	{
		t += 12;
		pc += 3;
		return;
	}
	
	pc = nn;
	
	t += 16;
}

void CPU::ld_ioc_a()			// LD (0xFF00 + C), A
{
	mmu.write(0xFF00 + r[C], r[A]);
	
	t += 8;
	pc += 1;
}

void CPU::ld_nn_a()				// LD (nn), A
{
	mmu.write(nn, r[A]);
	
	if (nn == rp(HL))
	{
		updateHLMEM();
	}
	
	t += 16;
	pc += 3;
}

void CPU::ld_a_ioc()			// LD A, (0xFF00 + C)
{
	r[A] = mmu.read(0xFF00 + r[C]);
	
	t += 8;
	pc += 1;
}

void CPU::ld_a_nn()				// LD A, (nn)
{
	r[A] = mmu.read(nn);
	
	t += 16;
	pc += 3;
}

void CPU::jp_nn()				// JP nn
{
	t += 16;
	pc = nn;
}

void CPU::di()					// DI
{
	ime = false;
	eiBool = false;
	
	t += 4;
	pc += 1;
}

void CPU::ei()					// EI
{
	eiBool = true;
	
	t += 4;
	pc += 1;
}

void CPU::call_cc_nn()			// CALL cc[y], nn
{
	pc += 3;
	
	if (cc(y) == 0)
	{
		t += 12;
		return;
	}
	
	call(nn);
	
	t += 24;
}

void CPU::push_rp2()			// PUSH rp2[p]
{
	push(rp2(p));
	
	t += 16;
	pc += 1;
}

void CPU::call_nn()				// CALL nn
{
	t += 24;
	pc += 3;
	
	call(nn);
}

void CPU::alu_n()				// alu[y] n
{
	alu(y, n);
	
	t += 8;
	pc += 2;
}

void CPU::rst()					// RST y*8
{
	t += 16;
	pc += 1;
	
	call(y * 8);
}

// CB-prefixed opcodes:

void CPU::rot_r()				// rot[y] r[z]
{
	if (z == R_HL)
	{
		unsigned char rhl = mmu.read(rp(HL));
		
		rot(y, rhl);
		mmu.write(rp(HL), rhl);
		
		t += 8;
	}
	
	else
	{
		rot(y, r[z]);
	}
	
	t += 8;
	pc += 2;
}

void CPU::bit()					// BIT y, r[z]
{
	cc(HC, 1);
	
	if (z == R_HL)
	{
		flags(F_Z, 0, getBit(y, mmu.read(rp(HL))), 0);
		
		t += 4;
	}
	
	else
	{
		flags(F_Z, 0, getBit(y, r[z]), 0);
	}
	
	t += 8;
	pc += 2;
}

void CPU::res()					// RES y, r[z]
{
	if (z == R_HL)
	{
		mmu.writeBit(y, 0, rp(HL));
		
		updateHLMEM();
		
		t += 8;
	}
	
	else
	{
		setBit(y, 0, r[z]);
	}
	
	t += 8;
	pc += 2;
}

void CPU::set()					// SET y, r[z]
{
	if (z == R_HL)
	{
		mmu.writeBit(y, 1, rp(HL));
		
		updateHLMEM();
		
		t += 8;
	}
	
	else
	{
		setBit(y, 1, r[z]);
	}
	
	t += 8;
	pc += 2;
}

void CPU::error()				// invalid opcode
{
	printf("Error, invalid opcode %02X at pc %04X\n", cpu.opcode, cpu.pc);
	
	Util::quit();
}



int CPU::cc(int index)
{
	switch (index)
	{
		case 0:							// NZ
		{
			return getBit(7, r[F]) ^ 1;
		}
		
		case 1:							// Z
		{
			return getBit(7, r[F]);
		}
		
		case 2:							// NCA
		{
			return getBit(4, r[F]) ^ 1;
		}
		
		case 3:							// CA
		{
			return getBit(4, r[F]);
		}
		
		case 4:							// NE
		{
			return getBit(6, r[F]);
		}
		
		case 5:							// HC
		{
			return getBit(5, r[F]);
		}
	}
	
	return -1;
}

void CPU::cc(int index, int value)
{
	switch (index)
	{
		case 1:							// Z
		{
			setBit(7, value, r[F]);
			break;
		}
		
		case 3:							// CA
		{
			setBit(4, value, r[F]);
			break;
		}
		
		case 4:							// NE
		{
			setBit(6, value, r[F]);
			break;
		}
		
		case 5:							// HC
		{
			setBit(5, value, r[F]);
			break;
		}
	}
}

void CPU::alu(int index, int operand)
{
	int prevA = r[A];
	
	switch (index)
	{
		case 0:										// ADD A
		{
			r[A] += operand;
			flags(F_Z | F_CA | F_HC, 0, prevA, operand);
			
			break;
		}
		
		case 1:										// ADC A
		{
			r[A] += operand + cc(CA);
			flags(F_Z | F_CA | F_HC, 6, prevA, operand);
			
			break;
		}
		
		case 2:										// SUB A
		{
			r[A] -= operand;
			flags(F_Z | F_CA | F_HC, 1, prevA, operand);
			
			break;
		}
		
		case 3:										// SBC A
		{
			r[A] -= operand + cc(CA);
			flags(F_Z | F_CA | F_HC, 7, prevA, operand);
			
			break;
		}
		
		case 4:										// AND A
		{
			r[A] &= operand;
			flags(F_Z | F_CA | F_HC, 2, prevA, operand);
			
			break;
		}
		
		case 5:										// XOR A
		{
			r[A] ^= operand;
			flags(F_Z | F_CA | F_HC, 3, prevA, operand);
			
			break;
		}
		
		case 6:										// OR A
		{
			r[A] |= operand;
			flags(F_Z | F_CA | F_HC, 4, prevA, operand);
			
			break;
		}
		
		case 7:										// CP A
		{
			flags(F_Z | F_CA | F_HC, 5, prevA, operand);
			
			break;
		}
	}
}

void CPU::rot(int index, unsigned char &operand)
{
	switch (index)
	{
		case 0:										// RLC
		{
			int prevBit7 = getBit(7, operand);
			
			operand <<= 1;
			setBit(0, prevBit7, operand);
			
			cc(CA, prevBit7);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 1:										// RRC
		{
			int prevBit0 = getBit(0, operand);
			
			operand >>= 1;
			setBit(7, prevBit0, operand);
			
			cc(CA, prevBit0);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 2:										// RL
		{
			int prevBit7 = getBit(7, operand);
			
			operand <<= 1;
			setBit(0, cc(CA), operand);
			
			cc(CA, prevBit7);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 3:										// RR
		{
			int prevBit0 = getBit(0, operand);
			
			operand >>= 1;
			setBit(7, cc(CA), operand);
			
			cc(CA, prevBit0);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 4:										// SLA
		{
			int prevBit7 = getBit(7, operand);
			
			operand <<= 1;
			
			cc(CA, prevBit7);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 5:										// SRA
		{
			int prevBit0 = getBit(0, operand);
			int prevBit7 = getBit(7, operand);
			
			operand >>= 1;
			setBit(7, prevBit7, operand);
			
			cc(CA, prevBit0);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
		
		case 6:										// SWAP
		{
			unsigned char prevReg(operand);
			
			setBit(0, getBit(4, prevReg), operand);
			setBit(1, getBit(5, prevReg), operand);
			setBit(2, getBit(6, prevReg), operand);
			setBit(3, getBit(7, prevReg), operand);
			
			setBit(4, getBit(0, prevReg), operand);
			setBit(5, getBit(1, prevReg), operand);
			setBit(6, getBit(2, prevReg), operand);
			setBit(7, getBit(3, prevReg), operand);
			
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			cc(CA, 0);
			
			break;
		}
		
		case 7:										// SRL
		{
			int prevBit0 = getBit(0, operand);
			
			operand >>= 1;
			setBit(7, 0, operand);
			
			cc(CA, prevBit0);
			flags(F_Z, 0, operand, 0);
			cc(HC, 0);
			
			break;
		}
	}
}

unsigned short CPU::rp(int index)
{
	unsigned short rp = 0;
	
	switch (index)
	{
		case 0:							// BC
		{
			rp = craftShort(r[B], r[C]);
			break;
		}
		
		case 1:							// DE
		{
			rp = craftShort(r[D], r[E]);
			break;
		}
		
		case 2:							// HL
		{
			rp = craftShort(r[H], r[L]);
			break;
		}
		
		case 3:							// SP
		{
			rp = sp;
			break;
		}
	}
	
	return rp;
}

void CPU::rp(int index, int value)
{
	switch (index)
	{
		case 0:							// BC
		{
			r[B] = subByte(1, value);
			r[C] = subByte(0, value);
			break;
		}
		
		case 1:							// DE
		{
			r[D] = subByte(1, value);
			r[E] = subByte(0, value);
			break;
		}
		
		case 2:							// HL
		{
			r[H] = subByte(1, value);
			r[L] = subByte(0, value);
			
			updateHLMEM();
			break;
		}
		
		case 3:							// SP
		{
			sp = value;
			break;
		}
	}
}

unsigned short CPU::rp2(int index)
{
	unsigned short rp2 = 0;
	
	switch (index)
	{
		case 0:							// BC
		
		case 1:							// DE
		
		case 2:							// HL
		{
			rp2 = rp(index);
			break;
		}
		
		case 3:							// AF
		{
			rp2 = craftShort(r[A], r[F]);
			break;
		}
	}
	
	return rp2;
}

void CPU::rp2(int index, int value)
{
	switch (index)
	{
		case 0:							// BC
		
		case 1:							// DE
		
		case 2:							// HL
		{
			rp(index, value);
			break;
		}
		
		case 3:							// AF
		{
			r[A] = subByte(1, value);
			r[F] = subByte(0, value);
			break;
		}
	}
}

void CPU::call(int address)
{
	rp(SP, rp(SP) - 2);
	
	mmu.write(sp, subByte(0, pc));
	mmu.write(sp + 1, subByte(1, pc));
	
	pc = address;
}

void CPU::ret()
{
	setByte(0, mmu.read(sp), pc);
	setByte(1, mmu.read(sp + 1), pc);
	
	rp(SP, rp(SP) + 2);
}

void CPU::push(int value)
{
	rp(SP, rp(SP) - 2);
	
	mmu.write(sp, subByte(0, value));
	mmu.write(sp + 1, subByte(1, value));
}

short CPU::pop()
{
	short stackVal = craftShort(mmu.read(sp + 1), mmu.read(sp));
	
	rp(SP, rp(SP) + 2);
	
	return stackVal;
}

void CPU::splitOpcode(unsigned char opc)
{
	y = (opc >> 3) & 7;
	
	z = opc & 7;
	
	p = (opc >> 4) & 3;
	
	q = (opc >> 3) & 1;
}

void CPU::updateMEMHL()
{
	mmu.write(rp(HL), r[R_HL]);
}

void CPU::updateHLMEM()
{
	r[R_HL] = mmu.read(rp(HL));
}

void CPU::flags(int flags, int operation, int fnum, int snum)
{
	switch (operation)
	{
		case 0:										// ADD flags
		{
			if (flags & F_Z)
			{
				bool zero = ((fnum + snum) % 256) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				bool hcarry = ((fnum & 0xF) + (snum & 0xF)) > 0xF;
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = (fnum + snum) > 255;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 1:										// SUB flags
		{
			if (flags & F_Z)
			{
				bool zero = fnum - snum == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 1);
			
			if (flags & F_HC)
			{
				bool hcarry = (snum & 0xF) > (fnum & 0xF);
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = snum > fnum;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 2:										// AND flags
		{
			if (flags & F_Z)
			{
				bool zero = (fnum & snum) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				cc(HC, 1);
			}
			
			if (flags & F_CA)
			{
				cc(CA, 0);
			}
			
			break;
		}
		
		case 3:										// XOR flags
		{
			if (flags & F_Z)
			{
				bool zero = (fnum ^ snum) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				cc(HC, 0);
			}
			
			if (flags & F_CA)
			{
				cc(CA, 0);
			}
			
			break;
		}
		
		case 4:										// OR flags
		{
			if (flags & F_Z)
			{
				bool zero = (fnum | snum) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				cc(HC, 0);
			}
			
			if (flags & F_CA)
			{
				cc(CA, 0);
			}
			
			break;
		}
		
		case 5:										// CP flags
		{
			if (flags & F_Z)
			{
				bool zero = (fnum - snum) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 1);
			
			if (flags & F_HC)
			{
				bool hcarry = (snum & 0xF) > (fnum & 0xF);
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = snum > fnum;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 6:										// ADC flags
		{
			if (flags & F_Z)
			{
				bool zero = ((fnum + snum) + cc(CA)) % 256 == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				bool hcarry = (fnum & 0x0F) + ((snum & 0x0F) + cc(CA)) > 0x0F;
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = (fnum + snum + cc(CA)) > 255;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 7:										// SBC flags
		{
			if (flags & F_Z)
			{
				bool zero = (fnum - (snum + cc(CA))) % 256 == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 1);
			
			if (flags & F_HC)
			{
				bool hcarry = ((snum & 0x0F) + cc(CA)) > (fnum & 0x0F);
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = (snum + cc(CA)) > fnum;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 8:										// ADD HL flags
		{
			if (flags & F_Z)
			{
				bool zero = ((fnum + snum) % 256) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				bool hcarry = ((fnum & 0x7FF) + (snum & 0x7FF)) > 0x7FF;
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = (fnum + snum) > 0xFFFF;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
		
		case 9:										// ADD SP, e8 flags
		{
			int finalSP = fnum + snum;
			
			int xorRes = (fnum ^ snum ^ finalSP);
			
			if (flags & F_Z)
			{
				bool zero = ((fnum + snum) % 256) == 0;
				
				cc(Z, zero ? 1 : 0);
			}
			
			cc(NE, 0);
			
			if (flags & F_HC)
			{
				bool hcarry = (xorRes & 0x0010) != 0;
				
				cc(HC, hcarry ? 1 : 0);
			}
			
			if (flags & F_CA)
			{
				bool carry = (xorRes & 0x0100) != 0;
				
				cc(CA, carry ? 1 : 0);
			}
			
			break;
		}
	}
}