#include <main.h>

SDL_Renderer *renderer;

SDL_Event e;

SDL_Window *window;

SDL_AudioSpec audioSpec;

Drivers::Drivers()
{
	
}

Drivers::Drivers(int scale)
{
	quit = false;
	
	init(scale);
}

void Drivers::init(int scale)
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	
	initVideo(scale);
	initAudio();
}

void Drivers::initVideo(int scale)
{
	window = SDL_CreateWindow("GrumpBoy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, G_W * scale, G_H * scale, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_RenderSetLogicalSize(renderer, G_W, G_H);
	
	if (window == NULL)
	{
		printf("Could not create window: %s\n", SDL_GetError());
		exit(1);
	}
}

void Drivers::initAudio()
{
	audioSpec = {0};
	
	audioSpec.freq = 44100;
	audioSpec.format = AUDIO_F32SYS;
	audioSpec.channels = 2;
	audioSpec.samples = APU::BUFFERSIZE;
	
	SDL_OpenAudio(&audioSpec, 0);
	
	SDL_PauseAudio(0);
}

void Drivers::fillAudio(float *buffer, int length)
{
	SDL_QueueAudio(1, buffer, length * sizeof(float));
	
	SDL_Delay(SDL_GetQueuedAudioSize(1) / (APU::BUFFERSIZE * 4));
}

void Drivers::setPixel(int x, int y, Color color)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 0xFF);
	
	SDL_RenderDrawPoint(renderer, x, y);
}

void Drivers::setPixels(Color gfx[])
{
	for (int y = 0; y < G_H; y++)
	{
		for (int x = 0; x < G_W; x++)
		{
			setPixel(x, y, gfx[x + (y * G_W)]);
		}
	}
	
	update();
}

void Drivers::clearDisplay()
{
	fillDisplay(Color(0, 0, 0));
}

void Drivers::fillDisplay(Color fill)
{
	for (int y = 0; y < G_H; y++)
	{
		for (int x = 0; x < G_W; x++)
		{
			setPixel(x, y, fill);
		}
	}
	
	update();
}

void Drivers::update()
{
	SDL_RenderPresent(renderer);
	
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
			case SDL_KEYDOWN:
			{
				switch (e.key.keysym.sym)
				{
					case SDLK_DOWN:
					{
						joypad.setDirectionBit(Joypad::DOWN, 0);
						
						break;
					}
					
					case SDLK_UP:
					{
						joypad.setDirectionBit(Joypad::UP, 0);
						
						break;
					}
					
					case SDLK_LEFT:
					{
						joypad.setDirectionBit(Joypad::LEFT, 0);
						
						break;
					}
					
					case SDLK_RIGHT:
					{
						joypad.setDirectionBit(Joypad::RIGHT, 0);
						
						break;
					}
					
					case SDLK_RETURN:
					{
						joypad.setActionBit(Joypad::START, 0);
						
						break;
					}
					
					case SDLK_LCTRL:
					{
						joypad.setActionBit(Joypad::SELECT, 0);
						
						break;
					}
					
					case SDLK_x:
					{
						joypad.setActionBit(Joypad::B, 0);
						
						break;
					}
					
					case SDLK_z:
					{
						joypad.setActionBit(Joypad::A, 0);
						
						break;
					}
				}
				
				break;
			}
			
			case SDL_KEYUP:
			{
				switch (e.key.keysym.sym)
				{
					case SDLK_DOWN:
					{
						joypad.setDirectionBit(Joypad::DOWN, 1);
						
						break;
					}
					
					case SDLK_UP:
					{
						joypad.setDirectionBit(Joypad::UP, 1);
						
						break;
					}
					
					case SDLK_LEFT:
					{
						joypad.setDirectionBit(Joypad::LEFT, 1);
						
						break;
					}
					
					case SDLK_RIGHT:
					{
						joypad.setDirectionBit(Joypad::RIGHT, 1);
						
						break;
					}
					
					case SDLK_RETURN:
					{
						joypad.setActionBit(Joypad::START, 1);
						
						break;
					}
					
					case SDLK_LCTRL:
					{
						joypad.setActionBit(Joypad::SELECT, 1);
						
						break;
					}
					
					case SDLK_x:
					{
						joypad.setActionBit(Joypad::B, 1);
						
						break;
					}
					
					case SDLK_z:
					{
						joypad.setActionBit(Joypad::A, 1);
						
						break;
					}
				}
				
				break;
			}
			
			case SDL_WINDOWEVENT:
			{
				if (e.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					quit = true;
				}
				
				break;
			}
			
			case SDL_QUIT:
			{
				quit = true;
				
				break;
			}
		}
	}
	
	if (quit)
	{
		exitGfx();
		
		Util::quit();
	}
}

void Drivers::exitGfx()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void setTitle()
{
	char title[16] = "";
	
	bool endTitle = false;
	
	for (int i = 0; (i < 16) && !endTitle; i++)
	{
		char c[] = { (char) mmu.read(0x134 + i) };
		
		if (c[0] != 0x00)
		{
			strncat(title, c, 1);
		}
		
		else
		{
			endTitle = true;
		}
	}
	
	SDL_SetWindowTitle(window, title);
}