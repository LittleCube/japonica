#include <main.h>

void NoiseChannel::init()
{
	EnvelopeChannel::init();
}

void NoiseChannel::triggerEvent()
{
	EnvelopeChannel::triggerEvent();
	
	lfsr = 0xFFFF;
}

void NoiseChannel::step()
{
	stepTimer();
	
	EnvelopeChannel::stepFrameSequencer();
}

void NoiseChannel::stepTimer()
{
	if (--timer == 0)
	{
		resetTimer();
		
		int xorResult = (lfsr & 1) ^ ((lfsr >> 1) & 1);
		
		lfsr >>= 1;
		
		lfsr &= ~(1 << 14) & 0x7FFF;
		
		lfsr |= xorResult << 14;
		
		if ((NRX3 >> 3) & 1)
		{
			lfsr &= ~(1 << 6);
			
			lfsr |= xorResult << 6;
		}
	}
}

void NoiseChannel::resetTimer()
{
	int clockShift = (NRX3 >> 4) & 0xF;
	
	if (clockShift < 14)
	{
		timer = decodeDivisor(NRX3 & 7) << clockShift;
	}
	
	else
	{
		timer = 0;
	}
}

unsigned char NoiseChannel::decodeDivisor(int divisorCode)
{
	unsigned char divisor;
	
	if (divisorCode == 0)
	{
		divisor = 8;
	}
	
	else
	{
		divisor = divisorCode << 4;
	}
	
	return divisor;
}

int NoiseChannel::getSample()
{
	return ~(lfsr & 1);
}