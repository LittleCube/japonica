#include <main.h>

void Channel2::init()
{
	ToneChannel::init();
	
	ADDR_NRX1 = 0xFF16;
	ADDR_NRX2 = 0xFF17;
	ADDR_NRX3 = 0xFF18;
	ADDR_NRX4 = 0xFF19;
	
	NRX1 = 0;
	NRX2 = 0;
	NRX3 = 0;
	NRX4 = 0;
}

float* Channel2::getSamples()
{
	int dutySelect = (NRX1 >> 6) & 3;
	
	float *samples = new float[2];
	
	if ((lengthCounter > 0) || (getBit(6, NRX4) == 0))
	{
		int dacInput = DUTYCYCLES[dutySelect][dutyIndex] * currentVolume;
		
		float dacOutput = (dacInput / 100.0f);
		
		if (getBit(1, mmu.memory[APU::NR51]))
		{
			int volume = mmu.memory[APU::NR50] & 7;
			
			samples[0] = dacOutput * volume;
		}
		
		else
		{
			samples[0] = 0;
		}
		
		if (getBit(5, mmu.memory[APU::NR51]))
		{
			int volume = (mmu.memory[APU::NR50] >> 4) & 7;
			
			samples[1] = dacOutput * volume;
		}
		
		else
		{
			samples[1] = 0;
		}
	}
	
	else
	{
		samples[0] = 0;
		samples[1] = 0;
	}
	
	return samples;
}