#include <main.h>

void WaveChannel::triggerEvent()
{
	Channel::triggerEvent();
	
	sampleIndex = 0;
}

void WaveChannel::updateLength(unsigned char newNRX1)
{
	if (lengthCounter == 0)
	{
		lengthCounter = 256 - newNRX1;
	}
}

void WaveChannel::step()
{
	stepTimer();
	
	Channel::stepFrameSequencer();
}

void WaveChannel::stepTimer()
{
	if (--timer == 0)
	{
		resetTimer();
		
		sampleIndex++;
		
		sampleIndex &= 31;
	}
}

void WaveChannel::resetTimer()
{
	int freq = ((NRX4 & 7) << 8) | NRX3;
	
	timer = (2048 - freq) * 2;
}

int WaveChannel::getSample()
{
	int sample;
	
	unsigned char sampleByte;
	
	sampleByte = mmu.memory[0xFF30 + (sampleIndex / 2)];
	
	if (sampleIndex % 2 == 0)
	{
		sample = (sampleByte >> 4) & 0xF;
	}
	
	else
	{
		sample = sampleByte & 0xF;
	}
	
	sample >>= getVolumeShift();
	
	return sample;
}

int WaveChannel::getVolumeShift()
{
	int shift;
	
	switch ((NRX2 >> 5) & 3)
	{
		case 0:
		{
			shift = 4;
			
			break;
		}
		
		case 1:
		{
			shift = 0;
			
			break;
		}
		
		case 2:
		{
			shift = 1;
			
			break;
		}
		
		case 3:
		{
			shift = 2;
			
			break;
		}
	}
	
	return shift;
}