#include <main.h>

CPU cpu;
Timer timer;
PPU ppu;
APU apu;
Joypad joypad;

Drivers drivers;

bool useBIOS = true;

int main(int argc, char** argv)
{
	if (argc > 2)
	{
		drivers = Drivers(stoi(argv[2]));
	}
	
	else
	{
		drivers = Drivers(4);
	}
	
	cpu.startBIOS();
	
	if (argc > 1)
	{
		mmu.loadROM(argv[1], false);
		
		setTitle();
	}
	
	while (true)
	{
		cpu.cycle();
		
		timer.clock(cpu.t);
		
		ppu.clock(cpu.t);
		
		apu.clock(cpu.t);
	}
}