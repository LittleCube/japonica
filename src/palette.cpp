#include <main.h>
/*
const Color Palette::TRUECOLORS[] = { Color(98, 111, 61),
                                      Color(71, 95, 76),
                                      Color(51, 84, 79),
                                      Color(51, 74, 73),
                                      Color(127, 111, 13) };
                                      // ^ this color is for LCD off
*/
const Color Palette::TRUECOLORS[] = { Color(0xFF, 0xA6, 0x30),
                                      Color(0xCE, 0x75, 0x00),
                                      Color(0xAA, 0x61, 0x00),
                                      Color(0x79, 0x45, 0x00),
                                      Color(127, 111, 13) };
                                      // ^ this color is for LCD off

Palette::Palette()
{
	setIndex(-1);
}

Palette::Palette(int newPalIndex)
{
	setIndex(newPalIndex);
}

void Palette::setIndex(int newPalIndex)
{
	palIndex = newPalIndex;
}

Color Palette::color(int color)
{
	unsigned char palRegister = mmu.read(palIndex);
	
	unsigned char subPal = 0;
	
	if (color < 4)
	{
		subPal = (palRegister >> (color * 2)) & 3;
	}
	
	else if (color == OFF)
	{
		subPal = OFF;
	}
	
	else
	{
		subPal = -1;
	}
	
	return trueColor(subPal);
}

Color Palette::trueColor(int color)
{
	return TRUECOLORS[color];
}