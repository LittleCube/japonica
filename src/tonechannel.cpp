#include <main.h>

const float ToneChannel::DUTYCYCLES[4][8] =
			{
				{ 0, 0, 0, 0, 0, 0, 0, 1 }, // 12.5%
				{ 1, 0, 0, 0, 0, 0, 0, 1 }, // 25%
				{ 1, 0, 0, 0, 0, 1, 1, 1 }, // 50%
				{ 0, 1, 1, 1, 1, 1, 1, 0 }  // 75%
			};

void ToneChannel::init()
{
	EnvelopeChannel::init();
	
	dutyIndex = 0;
}

void ToneChannel::step()
{
	stepTimer();
	
	EnvelopeChannel::stepFrameSequencer();
}

void ToneChannel::stepTimer()
{
	if (--timer == 0)
	{
		Channel::resetTimer();
		
		dutyIndex++;
		
		dutyIndex &= 7;
	}
}