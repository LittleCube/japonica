#include <main.h>
#include <chrono>
#include <thread>

using namespace chrono;

void Util::reset()
{
	cpu.initMMU();
	cpu.init();
	timer.init();
	
	ppu.init();
	apu.init();
	
	// TODO: add future scheduler init here
}

unsigned long Util::getTime()
{
	return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

void Util::msleep(long millisec)
{
	this_thread::sleep_for(chrono::milliseconds(millisec));
}

void Util::quit()
{
	printf("N: exiting...\n");
	
	exit(0);
}