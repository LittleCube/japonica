#include <main.h>
#include <cmath>

Tile::Tile()
{
	setIndex(-1);
}

Tile::Tile(unsigned short newIndex)
{
	setIndex(newIndex);
}

void Tile::setIndex(unsigned short newIndex)
{
	index = newIndex;
}

int Tile::getPixel(int x, int y)
{
	int pixel = 0;
	
	x = abs(x - 7);
	
	setBit(0, getBit(x, mmu.read(index + (y * 2))), pixel);
	setBit(1, getBit(x, mmu.read(index + (y * 2) + 1)), pixel);
	
	return pixel;
}